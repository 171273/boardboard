
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../components/HomeView.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import  {auth}  from '../firebase/firebaseInnit'


const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      requiresAuth:true
    }
  },
  {
    path: '/about',
    name: 'about',
  
    component: () => import(/* webpackChunkName: "about" */ '../components/AboutView.vue'),
    meta:{
      requiresAuth:true
    }
  },
  {
    path:'/login',
    name:'login',
    component: Login
  },
  {
    path:'/register',
    name:'register',
    component:Register
  }
 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next)=>{
   const requiresAuth=to.matched.some(record=> record.meta.requiresAuth)
  if( !auth.currentUser && requiresAuth ){
    next('/login')
    return;
  }
  else {
    next();
  }
})

export default router
